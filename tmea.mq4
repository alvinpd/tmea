/*
 * Trade Manager Expert Advisor v0.0
 *
 * Copyright (C) 2013, alvinpd <alvinpd09@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Disclaimer
 */

#property copyright "Copyright © 2013, alvinpd"
#property link      "alvinpd09@gmail.com"

#include <stdlib.mqh>
#include <stderror.mqh>
#include <WinUser32.mqh>

#define O_OP				0
#define O_SL				1
#define O_TP				2
#define O_BE				3
#define O_MAX				4

/*
 * External parameter settings
 */

extern string   Version.0.0         = "EA Settings:";
extern string   UserComment         = "Build v0.0";
extern bool     EmergencyCloseAll   = false;
extern bool     Debug               = false;

extern string   LabelInitialSL      = "Initial SL Settings:";
extern string   LabelSLMethod       = "1: Fixed, 2: ATR, 3: Risk";
extern int      SLMethod            = 1;
extern double   SLFixedPips         = 20;
extern string   LabelSLATRTF        = "0:Chart, 1:M1, 2:M5, 3:M15, 4:M30, 5:H1, 6:H4, 7:D1, 8:W1, 9:MN1";
extern int      SLATRTF             = 1;
extern int      SLATRPeriod         = 13;
extern double   SLATRRatio          = 2;
extern double   SLAccountRisk       = 1.0;  // Percent amount of account to risk
extern bool     SLAtBE              = true; // Move to BE after a certain number of pips
extern double   SLAtBEPips          = 30;   // Number of pips needed before SL moves to BE

extern string   LabelTrailingStop   = "Trailing Stop Settings:";
extern string   LabelTSMethod       = "0: None, 1: Fixed, 2: Dragon, 3: Trend";
extern int      TSMethod            = 1;
extern int      TSShift             = 0;    // The shift of the candle to check from current position

extern string   LabelTakeProfit     = "Take Profit Settings:";

extern string   LabelOS             = "Overlay Settings:";
extern color    SLColor             = Red;
extern int      SLStyle             = STYLE_DASHDOTDOT;
extern color    TPColor             = Green;
extern int      TPStyle             = STYLE_DASHDOTDOT;

/*
 * Global variable definitions
 */

double    Pip;
bool      Testing, Visual;
int       TF[10] = {0, 1, 5, 15, 30, 60, 240, 1440, 10080, 43200};

/*
 * Expert initialization function
 */
int init()
{
	/*
	 * Initialize global variables
	 */

	Testing = IsTesting();
	Visual = IsVisualMode();

	Pip = Point;
	if (Digits % 2 == 1)
		Pip *= 10;

	/*
	 * Normalize external parameter values
	 */

	SLMethod = clipi(SLMethod < 1 || SLMethod > 3, 1, SLMethod);
	SLFixedPips = nd(SLFixedPips * Pip, Digits);
	SLATRTF = clipi(SLATRTF < 0 || SLATRTF > 9, 1, SLATRTF);
	SLAccountRisk /= 100;
	SLAtBEPips = nd(SLAtBEPips * Pip, Digits);

	TSMethod = clipi(TSMethod < 0 || TSMethod > 3, 0, TSMethod);
	TSShift = clipi(TSShift < 0, 0, TSShift);

	return(0);
}

/*
 * Expert deinitialization function
 */
int deinit()
{
	return(0);
}

/*
 * Expert start function
 */
int start()
{
	int i, ot, type;
	double be, sldist = SLFixedPips;
	double order[O_MAX];

	RefreshRates();

	if (SLMethod == 2 /* ATR */) {
		sldist = iATR(Symbol(), TF[SLATRTF], SLATRPeriod, 0) * SLATRRatio;
	} else if (SLMethod == 3 /* Risk */) {
	}

	for (i = OrdersTotal() - 1; i >= 0; i--) {
		if (!OrderSelect(i, SELECT_BY_POS, MODE_TRADES))
			continue;

		if (OrderSymbol() != Symbol())
			continue;

		type = OrderType();

		/* Skip pending orders */
		if (type > OP_SELL)
			continue;

		ot = OrderTicket();
		order[O_OP] = OrderOpenPrice();
		order[O_SL] = OrderStopLoss();
		order[O_TP] = OrderTakeProfit();

		sl(ot, order, type, sldist);

		if (TSMethod != 0)
			ts();
	}

	return(0);
}

int sl(int ot, double &order[], int type, double dist)
{
	double stoploss = 0;

	if (order[O_SL] == 0.0) {
		/* A new order was just opened. Set the initial
		 * stop loss value.
		 */
		if (ot == OP_BUY)
			stoploss = order[O_BE] - dist;
		else if (ot == OP_SELL)
			stoploss = order[O_BE] + dist;
	}
	else if (SLAtBE && order[O_SL] != order[O_BE]) {
		/* Move stop loss to break even when SLAtBEPips is reached.
		 */
		if ((ot == OP_BUY && Bid >= (order[O_BE] + SLAtBEPips)) ||
		    (ot == OP_SELL && Ask <= (order[O_BE] - SLAtBEPips))
			stoploss = order[O_BE];
	}

	if (stoploss != 0)
		OrderModify(ot, order[O_OP], stoploss, order[O_TP], 0, SLColor);

	return(0);
}

int ts()
{
	return(0);
}

/*
 * Clip the value of an integer variable
 */
int clipi(bool chk, int def, int val)
{
	if (chk)
		return(def);
	else
		return(val);
}

double nd(double value, int digits)
{
	return(NormalizeDouble(value, digits));
}

/* End of file */
